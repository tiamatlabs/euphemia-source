# -*- coding: utf-8 -*-
import os
import requests
import traceback
import json
from flask import Flask, request

token = os.environ.get('FB_ACCESS_TOKEN')
app = Flask(__name__)

@app.route('/', methods=['GET', 'POST'])
def webhook():
    if request.method == 'POST':
        try:
            msg = "Desculpe, não entendi :/"
            data = json.loads(request.data.decode())
            text = data['entry'][0]['messaging'][0]['message']['text'].lower()
            sender = data['entry'][0]['messaging'][0]['sender']['id']
            print("Received: " + text)
            #Contexto
            if text=="kk":
                msg = "Eae men"
            if text=="do que voce gosta?":
                msg ="Jogos, navegar na internet e pizza :)"
            if text=="oi":
                msg = "oi meu chapa!"
            if text=="ola" or text=="eai" or text=="oi meu chapa":
                msg = "Olar ;)"
            if "youtube" in text:
                msg ="quer que eu procure um video? diz o nome."
            #Contexto
            print("Response message: " + msg)
            payload = {'recipient': {'id': sender}, 'message': {'text': msg}}
            r = requests.post('https://graph.facebook.com/v2.6/me/messages/?access_token=' + token, json=payload)
        except Exception as e:
            print(traceback.format_exc())
    elif request.method == 'GET':
        if request.args.get('hub.verify_token') == os.environ.get('FB_VERIFY_TOKEN'):
            return request.args.get('hub.challenge')
        return "Wrong Verify Token"
    return 'Nothing'

if __name__ == '__main__':
    app.run(debug=True)